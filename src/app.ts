window.onload = () => {

    const audioContext = new AudioContext();

    // const soundAnalyzer = new SoundAnalyzer(audioContext);
    // document.getElementById("btStart").onclick = () => {
    //     soundAnalyzer.displayFrequency(document.querySelector("#lblSoundData") as HTMLLabelElement);
    // };
    // document.getElementById("btStop").onclick = () => {
    //     soundAnalyzer.stop();
    // };

    const soundVisualizer = new SoundVisualizer(audioContext, false);
    document.getElementById("btStart").onclick = () => {
        soundVisualizer.visualizeWaveForm(document.querySelector("#visualizer") as HTMLCanvasElement);
    };
    document.getElementById("btStop").onclick = () => {
        soundVisualizer.stop();
    };
};
