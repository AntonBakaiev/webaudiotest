class SoundAnalyzer {
    private readonly fftSize = 2048;
    private readonly minSignalAmplitudeRms = 0.008; // if signal is weak - it is ignored
    private readonly rmsTreshold = 0.006;
    private readonly curvesOverlapTolerance = 0.001;
    private readonly minPitch = this.calculatePitch(this.minSignalAmplitudeRms);
    private readonly context: AudioContext;

    private microphoneSource: MediaStreamAudioSourceNode;
    private analyzer: AnalyserNode;
    private runningAnimationFrameId: number;
    private lastSignalRms = 0;
    private accessStringsUntilTime = 0;
    private analyzedStringsInLastFrame = false;

    private stringsOffsets: StringInfo[];
    private chart: CanvasJS.Chart;

    constructor(context: AudioContext) {
        this.context = context;
        this.stringsOffsets = this.initializeStringsOffsets(context);
        this.initializeGuitarStringsChart();
        this.connectMicrophoneToAnalyzer();
    }

    public displayFrequency(label: HTMLLabelElement) {
        if (this.runningAnimationFrameId != null) {
            this.stop();
        }

        // Данные в массиве `frequencyDataArray` - кривая. Значение каждого элемента массива варьируется от -1 до 1.
        // У каждой струны есть свой `offset`, характеризующий расстояние между одинаковыми участками кривой.
        // 1. Если корень из суммы квадратов данных в массиве, разделённой на количество данных,
        //    меньше `minSignalRms` - игнорируем этот звук.
        // 2. Нужно определить, на какую из струн (strings) больше всего похож звук.
        //    Проходим по всем струнам, и находим, насколько сильно отличаются значения точек текущей кривой,
        //    от соответствующих точек кривой каждой струны. Таким образом, если кривая текущего звука
        //    очень похожа на кривую струны d3, то `difference` для этой струны будет минимальной.
        // 3. Если анализ звука в течении 250 мс закончился - сортируем полученные разницы кривых по `difference`.
        //    Наименьший `difference` у наиболее подходящей струны, далее её запоминаем как `assumedString`.
        // 4. Теперь необходимо найти конкретную частоту полученной кривой путём перебора значений массива
        //    `frequencyDataArray` и определения разницы с выбранной струной (`assumedString`) с допуском
        //    погрешности `offset` от -10 до 10. Запоминаем наиболее подходящий `offset` (с минимальной
        //    `difference`). Частотой будет `context.sampleRate`, делённое на найденный `offset`.

        let displayCurrentFrequency: (time: number) => void = (time) => {
            this.runningAnimationFrameId = requestAnimationFrame(displayCurrentFrequency);

            let analyzedStringsInPreviousFrame = this.analyzedStringsInLastFrame;
            let currentOffset = 0;
            let currentDifference = 0;

            this.analyzer.fftSize = this.fftSize;
            let searchSize = this.analyzer.frequencyBinCount;
            let frequencyDataArray = new Float32Array(this.analyzer.fftSize);

            this.analyzer.getFloatTimeDomainData(frequencyDataArray);

            let squaresSum = 0;
            for (let timeDomainItem of frequencyDataArray) {
                squaresSum += timeDomainItem * timeDomainItem;
            }

            let rms = Math.sqrt(squaresSum / frequencyDataArray.length); // find signal's Root Main Square
            if (rms < this.minSignalAmplitudeRms) {
                return;
            }

            // only check for a new string if the volume goes up. Otherwise assume
            // that the string is the same as the last frame.
            if (rms > this.lastSignalRms + this.rmsTreshold) {
                this.accessStringsUntilTime = time + 250; // analyze strings signals next 250 ms of volume goes up
            }

            if (time < this.accessStringsUntilTime) {
                this.analyzedStringsInLastFrame = true;

                for (let stringInfo of this.stringsOffsets) {
                    currentOffset = stringInfo.offset;
                    currentDifference = 0;

                    // Reset difference for string, 'coz new analyzing cycle started
                    if (analyzedStringsInPreviousFrame === false) {
                        stringInfo.difference = 0;
                    }

                    for (let i = 0; i < searchSize; i++) {
                        currentDifference += Math.abs(frequencyDataArray[i] - frequencyDataArray[i + currentOffset]);
                    }

                    currentDifference /= searchSize;

                    stringInfo.difference += (currentDifference * currentOffset);
                }
            } else {
                this.analyzedStringsInLastFrame = false;
            }

            // If this is the first frame where we've not had to reassess strings
            // then we will order by the string with the largest number of matches.
            if (analyzedStringsInPreviousFrame === true && this.analyzedStringsInLastFrame === false) {
                this.stringsOffsets.sort((a, b) => a.difference - b.difference);
            }

            let assumedString = this.stringsOffsets[0];
            let searchRange = 10;
            let searchStart = assumedString.offset - searchRange;
            let searchEnd = assumedString.offset + searchRange;
            let actualFrequency = assumedString.offset;
            let smallestDifference = Number.POSITIVE_INFINITY;

            for (let bestOffsetMatch = searchStart; bestOffsetMatch < searchEnd; bestOffsetMatch++) {
                currentDifference = 0;

                for (let i = 0; i < searchSize; i++) {
                    currentDifference += Math.abs(frequencyDataArray[i] - frequencyDataArray[i + bestOffsetMatch]);
                }

                currentDifference /= searchSize;

                if (currentDifference < smallestDifference) {
                    smallestDifference = currentDifference;
                    actualFrequency = bestOffsetMatch;
                }

                // If curves almost identical - break the loop and assume found offset as needed.
                if (currentDifference < this.curvesOverlapTolerance) {
                    actualFrequency = bestOffsetMatch;
                    break;
                }
            }

            this.lastSignalRms = rms;

            let calculatedFrequency = this.context.sampleRate / actualFrequency;
            if (calculatedFrequency === 0) {
                return;
            }

            let maxAmplitude = Math.max.apply(Math, frequencyDataArray.map((a) => a));
            let pitch = this.calculatePitch(maxAmplitude);

            label.innerHTML = `String: ${assumedString.name}; Frequency: `
                + `${calculatedFrequency.toFixed(2)} Hz; Pitch: ${pitch} dBFS`;
            this.updateGuitarStringsChart(assumedString.name, pitch);
        };

        displayCurrentFrequency(performance.now());
    }

    public stop() {
        cancelAnimationFrame(this.runningAnimationFrameId);
    }

    private connectMicrophoneToAnalyzer() {
        navigator.getUserMedia({ audio: true },
            (stream) => {
                this.microphoneSource = this.context.createMediaStreamSource(stream);
                this.analyzer = this.context.createAnalyser();

                this.microphoneSource.connect(this.analyzer);
            },
            (error) => console.error(error.constraintName + ": " + error.message));
    }

    private initializeStringsOffsets(context: AudioContext): StringInfo[] {
        return Guitar.strings.map((gs) =>
            new StringInfo(gs.name, Math.round(context.sampleRate / gs.frequency), 0));
    }

    private initializeGuitarStringsChart() {
        this.chart = new CanvasJS.Chart("chartContainer", {
            theme: "dark2",
            animationEnabled: true,
            title: {
                text: "Guitar Strings",
            },
            axisX: {
                labelFontSize: 26,
            },
            axisY: {
                labelFontSize: 20,
            },
            data: [
                {
                    type: "column",
                    dataPoints: Guitar.strings.map((gs) => ({ label: gs.name, y: 0 })),
                },
            ],
        });

        this.chart.render();
    }

    private updateGuitarStringsChart(stringName: string, pitch: number) {
        let humanReadablePitch = pitch + Math.abs(this.minPitch);

        this.chart.options.data[0].dataPoints.find((s) => s.label === stringName).y = humanReadablePitch;
        this.chart.render();
    }

    // Getting pitch in dBFS: https://en.wikipedia.org/wiki/DBFS
    private calculatePitch(amplitude: number): number {
        return 20 * Math.log10(amplitude);
    }
}

// tslint:disable-next-line:max-classes-per-file
class StringInfo {
    public name: string;
    public offset: number;
    public difference: number;

    constructor(name: string, offset: number, difference: number) {
        this.name = name;
        this.offset = offset;
        this.difference = difference;
    }
}
