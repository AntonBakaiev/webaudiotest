class SoundVisualizer {
    // private readonly middleUIntValue: number = 128.0; // zero-frequency oscillation
    private readonly context: AudioContext;

    private microphoneSource: MediaStreamAudioSourceNode;
    private oscillatorSource: OscillatorNode;
    private analyzer: AnalyserNode;
    private runningAnimationFrameId: number;

    constructor(context: AudioContext, fromMicrophone: boolean) {
        this.context = context;
        if (fromMicrophone) {
            this.connectMicrophoneToAnalyzer();
        } else {
            this.connectOscillatorToAnalyzer("e2", "g3");
        }
    }

    public visualizeWaveForm(canvas: HTMLCanvasElement) {
        if (this.runningAnimationFrameId != null) {
            this.stop();
        }

        const canvasContext = canvas.getContext("2d");
        const canvasHeight = canvas.height;
        const canvasWidth = canvas.width;

        this.analyzer.fftSize = 2048;
        let bufferLength = this.analyzer.fftSize;
        let timeDomainDataArray = new Float32Array(bufferLength);

        canvasContext.clearRect(0, 0, canvasWidth, canvasHeight);

        let draw = () => {
            this.runningAnimationFrameId = requestAnimationFrame(draw);

            this.analyzer.getFloatTimeDomainData(timeDomainDataArray);
            canvasContext.fillStyle = "rgb(30, 30, 30)";
            canvasContext.setLineDash([5, 15]);
            canvasContext.fillRect(0, 0, canvasWidth, canvasHeight);
            canvasContext.lineWidth = 1;
            canvasContext.strokeStyle = "rgb(0, 165, 168)";

            let yAxisHeight = canvasHeight / 2;

            canvasContext.beginPath();
            canvasContext.moveTo(0, yAxisHeight);
            canvasContext.lineTo(canvasWidth, yAxisHeight);
            canvasContext.stroke();

            canvasContext.setLineDash([]);
            canvasContext.lineWidth = 2;
            canvasContext.beginPath();
            let sliceWidth = canvasWidth * 1.0 / bufferLength;
            let sliceX = 0;

            for (let i = 0; i < bufferLength; i++ , sliceX += sliceWidth) {
                let sliceY = yAxisHeight + timeDomainDataArray[i] * yAxisHeight / 4;

                if (i === 0) {
                    canvasContext.moveTo(sliceX, sliceY);
                } else {
                    canvasContext.lineTo(sliceX, sliceY);
                }
            }

            canvasContext.lineTo(canvas.width, yAxisHeight);
            canvasContext.stroke();
        };

        draw();
    }

    public visualizeFrequencyBar(canvas: HTMLCanvasElement) {
        if (this.runningAnimationFrameId != null) {
            this.stop();
        }

        const canvasContext = canvas.getContext("2d");
        const canvasHeight = canvas.height;
        const canvasWidth = canvas.width;

        this.analyzer.fftSize = 256;
        let bufferLength = this.analyzer.frequencyBinCount;
        let frequencyDataArray = new Uint8Array(bufferLength);

        canvasContext.clearRect(0, 0, canvasWidth, canvasHeight);

        let draw = () => {
            this.runningAnimationFrameId = requestAnimationFrame(draw);

            this.analyzer.getByteFrequencyData(frequencyDataArray);
            canvasContext.fillStyle = "rgb(0, 0, 0)";
            canvasContext.fillRect(0, 0, canvasWidth, canvasHeight);

            let barWidth = (canvasWidth / bufferLength) * 2.5; // to shift low frequency sounds and make it noticeable
            let barHeight;
            let barX = 0;

            for (let i = 0; i < bufferLength; i++ , barX += barWidth + 1) {
                barHeight = frequencyDataArray[i] / 2;
                canvasContext.fillStyle = "rgb(" + (barHeight + 100) + ",50,50)";
                canvasContext.fillRect(barX, canvasHeight - barHeight / 2, barWidth, barHeight);
            }
        };

        draw();
    }

    public stop() {
        cancelAnimationFrame(this.runningAnimationFrameId);
    }

    private connectMicrophoneToAnalyzer() {
        navigator.getUserMedia({ audio: true },
            (stream) => {
                this.microphoneSource = this.context.createMediaStreamSource(stream);
                this.analyzer = this.context.createAnalyser();

                this.microphoneSource.connect(this.analyzer);
            },
            (error) => console.error(error.constraintName + ": " + error.message));
    }

    private connectOscillatorToAnalyzer(...guitarStrings: string[]) {
        this.analyzer = this.context.createAnalyser();

        for (const guitarString of guitarStrings) {
            const frequency = Guitar.getString(guitarString).frequency;
            this.oscillatorSource = this.context.createOscillator();
            this.oscillatorSource.frequency.setValueAtTime(frequency, this.context.currentTime);

            this.oscillatorSource.connect(this.analyzer);
            this.oscillatorSource.start();
        }
        // this.oscillatorSource = this.context.createOscillator();
        // this.oscillatorSource.frequency.setValueAtTime(frequency, this.context.currentTime);

        // this.oscillatorSource.connect(this.analyzer);
        // this.oscillatorSource.start();
    }
}
