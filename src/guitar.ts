class Guitar {
    public static readonly strings: GuitarString[] = [
        { name: "E2", frequency: 82.4069 },
        { name: "A2", frequency: 110.000 },
        { name: "D3", frequency: 146.832 },
        { name: "G3", frequency: 195.998 },
        { name: "B3", frequency: 246.932 },
        { name: "E4", frequency: 329.628 },
    ];

    public static getString(name: string): GuitarString {
        return this.strings.find((gs) => gs.name === name.toUpperCase());
    }
}

// tslint:disable-next-line:max-classes-per-file
class GuitarString {
    public readonly name: string;
    public readonly frequency: number;
}
